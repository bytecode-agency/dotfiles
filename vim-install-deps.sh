#!/bin/sh

mkdir ~/.vim/bundle

cd ~
cd .vim
cd bundle

git clone https://github.com/LaTeX-Box-Team/LaTeX-Box.git
git clone https://github.com/vim-scripts/SQLComplete.vim.git
git clone https://github.com/jiangmiao/auto-pairs.git
git clone https://github.com/metakirby5/codi.vim.git
git clone https://github.com/ekalinin/Dockerfile.vim.git
git clone https://github.com/mattn/emmet-vim.git
git clone https://github.com/othree/html5.vim.git
git clone https://github.com/othree/javascript-libraries-syntax.vim.git
git clone https://github.com/itchyny/lightline.vim.git
git clone https://github.com/scrooloose/nerdcommenter.git
git clone https://github.com/kien/rainbow_parentheses.vim.git
git clone https://github.com/cakebaker/scss-syntax.vim.git
git clone https://github.com/vim-syntastic/syntastic.git
git clone https://github.com/godlygeek/tabular.git
git clone https://github.com/leafgarland/typescript-vim.git
git clone https://github.com/alvan/vim-closetag.git
git clone https://github.com/tpope/vim-commentary.git
git clone https://github.com/ap/vim-css-color.git
git clone https://github.com/easymotion/vim-easymotion.git
git clone https://github.com/tpope/vim-eunuch.git
git clone https://github.com/tpope/vim-fugitive.git
git clone https://github.com/airblade/vim-gitgutter.git
git clone https://github.com/fatih/vim-go.git
git clone https://github.com/nathanaelkane/vim-indent-guides.git
git clone https://github.com/pangloss/vim-javascript.git
git clone https://github.com/maksimr/vim-jsbeautify.git
git clone https://github.com/elzr/vim-json.git
git clone https://github.com/mxw/vim-jsx.git
git clone https://github.com/neoclide/vim-jsx-improve.git
git clone https://github.com/vim-latex/vim-latex.git
git clone https://github.com/plasticboy/vim-markdown.git
git clone https://github.com/amadeus/vim-mjml.git
git clone https://github.com/moll/vim-node.git
git clone https://github.com/sheerun/vim-polyglot.git
git clone https://github.com/prettier/vim-prettier.git
git clone https://github.com/tpope/vim-projectionist.git
git clone https://github.com/tpope/vim-repeat.git
git clone https://github.com/vitalk/vim-simple-todo.git
git clone https://github.com/honza/vim-snippets.git
git clone https://github.com/tpope/vim-surround.git
git clone https://github.com/janko-m/vim-test.git
git clone https://github.com/bronson/vim-trailing-whitespace.git
git clone https://github.com/tpope/vim-unimpaired.git
git clone https://github.com/ajh17/VimCompletesMe.git
git clone https://github.com/lervag/vimtex.git
git clone https://github.com/trkw/yarn.vim.git
git clone https://github.com/gcorne/vim-sass-lint.git
